import { BaseEntity } from './BaseEntity';

export class Pais implements BaseEntity {
	constructor(
		public codigo?: String, 
	 	public nombre?: string,
		public zona?: BaseEntity[],
		) {}
}