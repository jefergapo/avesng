import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AvesService } from './aves.service';

import { Ave } from './ave.model';
import { Pais } from './pais.model';
import { Zona } from './zona.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  	title = 'app';
	
	aves: Ave[];
	ave: Ave = new Ave();
	zonas: Zona[];
	paises: Pais[];
   	statusCode: number;
	updating: boolean;

	constructor(private avesservice: AvesService) {}
	

	ngOnInit() {
	  	this.loadAll();
   }
	
	loadAll() {
	  	this.getAllZonas();
		this.getAllPaises();
		this.getAllAves();
		this.updating = false;
		this.ave = new Ave();
   }

	getAllAves() {
        this.avesservice.getAll()
	    .subscribe(
			data => this.aves = data,
			errorCode =>  this.statusCode = errorCode);   
   }
	
	getAve(codigo: string) {
		this.updating = true
        this.avesservice.getByCodigo(codigo)
	    .subscribe(
			data => this.ave = data,
			errorCode =>  this.statusCode = errorCode);   
   }

	buscar(params) {
		this.avesservice.buscar(params.zona, params.nombre)
		.subscribe(
			data => this.aves = data,
			errorCode =>  this.statusCode = errorCode); 
	  
   }
	guardar() {
		console.log(this.ave);
		this.avesservice.create(this.ave)
		.subscribe(
			data => this.reload(data),
			errorCode =>  this.statusCode = errorCode);
		this.loadAll(); 
   }
	
	editar() {
		console.log(this.ave);
		this.avesservice.update(this.ave)
		.subscribe(
			data => this.reload(data),
			errorCode =>  this.statusCode = errorCode);
   }

	eliminar(codigo: string) {
		console.log(codigo);
		this.avesservice.delete(codigo)
		.subscribe(
			data => this.reload(data),
			errorCode =>  this.statusCode = errorCode);
		
   }

	getAllZonas() {
        this.avesservice.getAllZonas()
	    .subscribe(
			data => this.zonas = data,
			errorCode =>  this.statusCode = errorCode)
   }

	getAllPaises() {
        this.avesservice.getAllPaises()
	    .subscribe(
			data => this.paises = data,
			errorCode =>  this.statusCode = errorCode);   
   }
	
	trackPaisesByCodigo(index: number, item: Pais) {
        return item.codigo;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.codigo === selectedVals[i].codigo) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
	
	reload(object: number) {
		console.log(object);
        this.loadAll();
    }
}
