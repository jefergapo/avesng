import { BaseEntity } from './BaseEntity';

export class Zona implements BaseEntity {
	constructor(
		public codigo?: String, 
	 	public nombre?: string, 
		) {}
}