import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Ave } from './ave.model';
import { Pais } from './pais.model';
import { Zona } from './zona.model';

@Injectable()
export class AvesService {
	
	apiUrl = "http://localhost:8080/api/aves";
	
  	constructor(private http: Http) { }
	
	getAll(): Observable<Ave[]> {
        return this.http.get(this.apiUrl)
	   .map(this.extractData)
	   .catch(this.handleError);
    }
	
	getByCodigo(codigo: string): Observable<Ave> {
	let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
	let options = new RequestOptions({ headers: cpHeaders });
	console.log(this.apiUrl +"/"+ codigo);
	return this.http.get(this.apiUrl +"/"+ codigo)
	   .map(this.extractData)
	   .catch(this.handleError);
    }	
	
	create(ave: Ave):Observable<number> {
	let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.apiUrl, ave, options)
               .map(success => success.status)
               .catch(this.handleError);
    }
	
	update(ave: Ave):Observable<number> {
	let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.put(this.apiUrl +"/"+ ave.codigo, ave, options)
               .map(success => success.status)
               .catch(this.handleError);
    }
	
    delete(codigo: string): Observable<number> {
	let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
	let options = new RequestOptions({ headers: cpHeaders });
	return this.http.delete(this.apiUrl +"/"+ codigo)
	       .map(success => success.status)
               .catch(this.handleError);
    }
	
	buscar(zona: string, nombre: string): Observable<Ave[]> {
	let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
	let options = new RequestOptions({ headers: cpHeaders });
	console.log(this.apiUrl +"/buscar");
	return this.http.get(this.apiUrl +"/buscar?nombre=" + nombre + "&zona=" + zona)
	   .map(this.extractData)
	   .catch(this.handleError);
    }
	
	getAllPaises(): Observable<Pais[]> {
        return this.http.get(this.apiUrl + "/paises")
	   .map(this.extractData)
	   .catch(this.handleError);
    }

	getAllZonas(): Observable<Zona[]> {
        return this.http.get(this.apiUrl + "/paises/zonas")
	   .map(this.extractData)
	   .catch(this.handleError);
    }

	private extractData(res: Response) {
		let body = res.json();
        return body;
    }
	
    private handleError (error: Response | any) {
		console.error(error.message || error);
		return Observable.throw(error.status);
    }

}
